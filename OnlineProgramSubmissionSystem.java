package MyPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class OnlineProgramSubmissionSystem {
	public static void main(String args[]){
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://eee-sel-as02.eee.ntu.edu.sg:7777";
		driver.get(baseUrl);
		

		// Test LogIn using correct password
		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("root");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("root8888");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		driver.findElement(By.partialLinkText("Log")).click();
        

		// test LogIn using wrong password

		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("root");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();

		// test forget password
		driver.findElement(By.partialLinkText("Forgot")).click();

		// Go to Login page again
		driver.findElement(By.partialLinkText("Login")).click();

		// test LogIn using email address

		/*driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys(
				"liuq0017@e.ntu.edu.sg");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("liuq0017");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();*/
		
		//Re-login again using correct password
		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("root");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("root8888");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		
		//move the mouse to the "try section"
        WebElement weTry = driver.findElement(By.xpath("//table/tbody/tr[1]/td[1]"));
        Actions builder = new Actions(driver);
        Action mouseOverTry = builder.moveToElement(weTry).build();
        mouseOverTry.perform();
        
        //Find By Tutorial
        driver.findElement(By.partialLinkText("By Tutorial")).click();
        
        //Go back to Main
        driver.findElement(By.partialLinkText("Main")).click();
        
       
        
     
        
        
        
       
        
		
		
		
		
	}

}
