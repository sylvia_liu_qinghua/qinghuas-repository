package MyPackage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class TestSubmissionJavaCompilationError {
public static void main(String[]args){
		
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://eee-sel-as02.eee.ntu.edu.sg:7777";
		driver.get(baseUrl);
		

		// Test LogIn using correct password
		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("tinstr");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("test");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
	
		//since the problem's already setup, no need to set up again, but just in case
		//WebElement weSetUp = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]"));
        //Actions builder = new Actions(driver);
        //Action mouseOverSetUp = builder.moveToElement(weSetUp).build();
        //mouseOverSetUp.perform();
        //driver.findElement(By.partialLinkText("Problem")).click(); 
        
        //driver.findElement(By.xpath("//input[@type='file']")).sendKeys("/Users/liuqinghua/Desktop/test-7/problems/CreateProblem9001-Add2IntegersJava.txt");
        //driver.findElement(By.cssSelector("input[type = 'submit']")).click();
        
        WebElement weTry = driver.findElement(By.xpath("//table/tbody/tr[1]/td[1]"));
        Actions builderTry = new Actions(driver);
        Action mouseOverTry = builderTry.moveToElement(weTry).build();
        mouseOverTry.perform();
        driver.findElement(By.partialLinkText("By Problem")).click(); 
        driver.findElement(By.partialLinkText("TRY")).click(); 
        
        driver.findElement(By.xpath("//input[@type='file']")).sendKeys("/Users/liuqinghua/Desktop/test-7/Solutions/CompileError/Add2Integer.java");
        driver.findElement(By.cssSelector("input[type = 'submit']")).click();
        
        WebElement weTryAgain = driver.findElement(By.xpath("//table/tbody/tr[1]/td[1]"));
        Actions builderTryAgain = new Actions(driver);
        Action mouseOverTryAgain = builderTryAgain.moveToElement(weTryAgain).build();
        mouseOverTryAgain.perform();
        
        //Needs to add delay though
        driver.findElement(By.partialLinkText("By Problem")).click(); 
        
       
        
        
	}

}
