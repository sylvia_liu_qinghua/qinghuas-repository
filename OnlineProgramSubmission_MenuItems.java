package MyPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class OnlineProgramSubmission_MenuItems {
	public void main(String[]args){
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://eee-sel-as02.eee.ntu.edu.sg:7777";
		driver.get(baseUrl);
		

		// Test LogIn using correct password
		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("root");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("root8888");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		driver.findElement(By.partialLinkText("Log")).click();
		
		//move the mouse to the "try section"
        WebElement weTry = driver.findElement(By.xpath("//table/tbody/tr[1]/td[1]"));
        Actions builder = new Actions(driver);
        Action mouseOverTry = builder.moveToElement(weTry).build();
        mouseOverTry.perform();
        
        driver.findElement(By.partialLinkText("By Tutorial")).click(); 
        driver.findElement(By.partialLinkText("Main")).click();
        mouseOverTry.perform();
        driver.findElement(By.partialLinkText("By Problem")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        
        //Grade section
        WebElement weGrade = driver.findElement(By.xpath("//table/tbody/tr[1]/td[2]"));
        Actions builderGrade = new Actions(driver);
        Action mouseOverGrade = builderGrade.moveToElement(weGrade).build();
        mouseOverGrade.perform();
        
        driver.findElement(By.partialLinkText("By Problem")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("By User")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        
        //report section
        
        WebElement weReport = driver.findElement(By.xpath("//table/tbody/tr[1]/td[3]"));
        Actions builderReport = new Actions(driver);
        Action mouseOverReport = builderReport.moveToElement(weReport).build();
        mouseOverReport.perform();
        
        driver.findElement(By.partialLinkText("By User")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Summary")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        
        //setup section
        
        WebElement weSetup = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]"));
        Actions builderSetup = new Actions(driver);
        Action mouseOverSetup = builderSetup.moveToElement(weSetup).build();
        mouseOverSetup.perform();
        
        driver.findElement(By.partialLinkText("Courses")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Problems")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Tutorials")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Enroll Users")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        
        //Database section
        WebElement weDatabase = driver.findElement(By.xpath("//table/tbody/tr[1]/td[5]"));
        Actions builderDatabase = new Actions(driver);
        Action mouseOverDatabase = builderDatabase.moveToElement(weDatabase).build();
        mouseOverDatabase.perform();
        
        driver.findElement(By.partialLinkText("Users")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("course")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Tutorials")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Problems")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        driver.findElement(By.partialLinkText("Message")).click();
        driver.findElement(By.partialLinkText("Main")).click();
        
        
        
        
        
        
        
        
        

	}

}
