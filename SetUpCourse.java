package MyPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class SetUpCourse {
	
	public static void main(String[]args){
		
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://eee-sel-as02.eee.ntu.edu.sg:7777";
		driver.get(baseUrl);
		

		// Test LogIn using correct password
		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("root");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("root8888");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		
		WebElement weSetUp = driver.findElement(By.xpath("//table/tbody/tr[1]/td[4]"));
        Actions builder = new Actions(driver);
        Action mouseOverSetUp = builder.moveToElement(weSetUp).build();
        mouseOverSetUp.perform();
        
        driver.findElement(By.partialLinkText("Course")).click(); 
		
	}


}
