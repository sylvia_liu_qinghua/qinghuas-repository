package MyPackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestOnlineSubmission_ForgetPassWord {
	public static void main(String args[]) {
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "https://eee-sel-as02.eee.ntu.edu.sg";
		driver.get(baseUrl);

		// Test LogIn using correct password
		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("liuq0017");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("liuq0017");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		driver.findElement(By.partialLinkText("Log")).click();

		// test LogIn using wrong password

		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys("liuq0017");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("123456");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();

		// test forget password
		driver.findElement(By.partialLinkText("Forgot")).click();

		// Go to Login page again
		driver.findElement(By.partialLinkText("Login")).click();

		// test LogIn using email address

		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys(
				"liuq0017@e.ntu.edu.sg");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("liuq0017");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();

		// Find Settings button
		driver.findElement(By.partialLinkText("Settings")).click();

		// Try to change the email using an old email address
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys("liuq0017@e.ntu.edu.sg");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();

		// clear the change email field and input a new email address

		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys("silverway1992@gmail.com");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		driver.findElement(By.partialLinkText("Log")).click();

		// Try login using new email address

		driver.findElement(By.id("loginIDOrEmail")).clear();
		driver.findElement(By.id("loginIDOrEmail")).sendKeys(
				"silverway1992@gmail.com");
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("liuq0017");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();

		//change the email back
		driver.findElement(By.partialLinkText("Settings")).click();
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys("liuq0017@e.ntu.edu.sg");
		driver.findElement(By.cssSelector("input[type = 'submit']")).click();
		
		//change password using wrong current password
		driver.findElement(By.id("oldPassword")).sendKeys("123456");
		
		
		
		
		
	}

}
